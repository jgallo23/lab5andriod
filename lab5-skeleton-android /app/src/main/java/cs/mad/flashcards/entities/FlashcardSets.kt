package cs.mad.flashcards.entities

import androidx.room.Entity

@Entity(tableName = "flashcardSetsTB")
class FlashcardSets (
    val title: String,
    val id: Long? = null
        )