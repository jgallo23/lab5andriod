package cs.mad.flashcards.entities

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FlashcardVM(application: Application): AndroidViewModel(application) {

    private val readAllData: LiveData<List<Flashcards>>
    private val repository: ProjectRepo

    init {
        val FlashcardsDAO = ProjectDB.getDatabase(application).flashcardsDAO()
        repository = ProjectRepo(FlashcardsDAO)
        readAllData = repository.readAllData
    }

    fun addFlashcards(flashcards: Flashcards){
        viewModelScope.launch(Dispatchers.IO) {
            repository.addFlashcards(flashcards)
        }
    }
}