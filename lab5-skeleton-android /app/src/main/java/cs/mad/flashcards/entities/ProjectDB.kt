package cs.mad.flashcards.entities

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import java.security.AccessControlContext
import java.time.Instant

@Database(entities = [Flashcards::class], version = 1, exportSchema = false)
abstract class ProjectDB: RoomDatabase() {

    abstract fun flashcardsDAO(): FlashcardsDAO

    companion object{
        @Volatile
        private var INSTANCE: ProjectDB? = null

        fun getDatabase(context: Context): ProjectDB{
            val tempInstance = INSTANCE
            if(tempInstance != null){
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ProjectDB::class.java,
                    "flashcardTB"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}