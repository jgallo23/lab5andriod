package cs.mad.flashcards.entities

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface FlashcardSetsDAODAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addFlashcard(flashcardSets: FlashcardSets)

    @Query("SELECT * FROM flashcardSetsTB ORDER BY title ASC")
    fun readAllData(): LiveData<List<FlashcardSets>>
}