package cs.mad.flashcards.entities

import androidx.room.Entity

@Entity(tableName = "flashcardsTB")
class Flashcards (
    val question: String,
    val answer: String
        )
