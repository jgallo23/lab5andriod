package cs.mad.flashcards.entities

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface FlashcardsDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addFlashcard(flashcards: Flashcards)

    @Query("SELECT * FROM flashcardsTB ORDER BY question ASC")
    fun readAllData(): LiveData<List<Flashcards>>
}