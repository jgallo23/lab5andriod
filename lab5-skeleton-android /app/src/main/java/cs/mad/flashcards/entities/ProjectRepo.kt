package cs.mad.flashcards.entities

import androidx.lifecycle.LiveData

class ProjectRepo(private val FlashcardDAO: FlashcardsDAO) {

    val readAllData: LiveData<List<Flashcards>> = FlashcardDAO.readAllData()

    suspend fun addFlashcards(flashcards: Flashcards){
        FlashcardDAO.addFlashcard(flashcards)
    }
}